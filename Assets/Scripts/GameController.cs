﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameController : MonoBehaviour {

    public static int score;
    public PlayerFPSController fpsController;
    public Text finalScoreText;
    public Text inGameScoreText;
    public GameObject gameOverWindow;

    private void Start()
    {
        gameOverWindow.SetActive(false);
    }
    private void Update()
    {
        if (fpsController.playerIsDead())
        {
            finalScoreText.text = "SCORE: " + score.ToString("00000");
            gameOverWindow.SetActive(true);
            if (Input.GetKey(KeyCode.Return))
            {
                restartGame();
            }
        }
        else
        {
            inGameScoreText.text = "SCORE: " + score.ToString("00000");
        }
    }

    public static void restartGame()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
           SceneManager.LoadScene("SampleScene");
    }
}
