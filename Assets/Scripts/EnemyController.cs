﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    private CharacterHealth characterHealth;
    //  private GameObject enemy;
    public int points;
	// Use this for initialization
	void Start () {
        characterHealth = GetComponent<CharacterHealth>();
    }
	
	// Update is called once per frame
	void Update () {
        //this.gameObject.SetActive(true);
        if (characterHealth.isDead)
        {
            GameController.score += points;
            // Destroy(gameObject, 0.2f);
            characterHealth.revive();
            characterHealth.Health_After_Revive();
            
            this.gameObject.SetActive(false);
           //enabled = false;
        }
	}

    public void respawner(Vector3 pos)
    {
        this.gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;//adios blood clones
    }
}
