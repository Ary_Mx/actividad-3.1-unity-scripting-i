﻿using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class PlayerFPSController : MonoBehaviour {

    public enum PLAYER_STATES { IDLE, IDLE_FIRE, IDLE_AIM, IDLE_AIM_FIRE, WALK, WALK_FIRE, WALK_AIM, WALK_AIM_FIRE, RELOAD, RUN, JUMP }
    public PLAYER_STATES playerCurrentState;
    public RecoilConfig recoilConfig;

    private InputData input;
    private CharacterMovement characterMovement;
    private FireGun gun;
    private GunAiming gunAim;
    private HeadBobbingController headBobbingController;
    private MouseLook mouseLook;
    private CharacterHealth characterHealth;
    private float recoilMultiplier;


    void Start () {
        // Find the object with name Capsule and disable its meshrenderer
        GameObject.Find("Capsule").GetComponent<MeshRenderer>().enabled = false;
        characterMovement = GetComponent<CharacterMovement>();
        gun = GetComponentInChildren<FireGun>();
        gunAim = GetComponentInChildren<GunAiming>();
        headBobbingController = GetComponent<HeadBobbingController>();
        mouseLook = GetComponent<MouseLook>();
        characterHealth = GetComponent<CharacterHealth>();

        playerCurrentState = PLAYER_STATES.IDLE;
        headBobbingController.setBobbingConfig(playerCurrentState, 0f);
    }
	

	void Update () {

        if (characterHealth.isDead)
            return;

        // Get input from player//aquiF
            input.getInput();

           

        // Override input based on FSM states restrictions
        FSMmanager();

        // Apply Movement
        characterMovement.moveCharacter(input.hMovement, input.vMovement, input.jump, input.dash);

        //Apply Rotation
        mouseLook.verticalAxis = input.verticalMouse;
        mouseLook.horizontalAxis = input.horizontalMouse;

        // Aim 
        gunAim.Aiming(input.aimButtonDown, input.aimButtonUp);

        // Fire
        gun.triggerPulled = input.fireButton;
        gun.recoilerMultiplier = recoilMultiplier;

        // HeadBobbing
        headBobbingController.setBobbingConfig(playerCurrentState, characterMovement.currentNormalizedSpeed);
    }

    public bool playerIsDead() {

        return characterHealth.isDead;
    }

    private void FSMmanager() {

        switch (playerCurrentState) {

            case PLAYER_STATES.IDLE:
                idleState();
                break;

            case PLAYER_STATES.IDLE_FIRE:
                idleFireState();
                break;

            case PLAYER_STATES.IDLE_AIM:
                idleAimState();
                break;

            case PLAYER_STATES.IDLE_AIM_FIRE:
                idleAimFireState();
                break;

            case PLAYER_STATES.WALK:
                walkState();
                break;

            case PLAYER_STATES.WALK_FIRE:
                walkFireState();
                break;

            case PLAYER_STATES.WALK_AIM:
                walkAimState();
                break;

            case PLAYER_STATES.WALK_AIM_FIRE:
                walkAimFireState();
                break;

            case PLAYER_STATES.RUN:
                runState();
                break;

            case PLAYER_STATES.JUMP:
                jumpState();
                break;

            case PLAYER_STATES.RELOAD:
                reload();
                break;
        }
    }


    private bool wantsToMove() {

        return Mathf.Abs(input.hMovement) + Mathf.Abs(input.vMovement) > 0f;
    }

    private bool breakStates() {

        if (input.dash) {
            playerCurrentState = PLAYER_STATES.RUN;

        }
        else if (input.jump) {
            playerCurrentState = PLAYER_STATES.JUMP;

        }
        else if (input.reload) {
            playerCurrentState = PLAYER_STATES.RELOAD;

        }

        return playerCurrentState == PLAYER_STATES.RUN ||
               playerCurrentState == PLAYER_STATES.JUMP ||
               playerCurrentState == PLAYER_STATES.RELOAD;
    }


    private void idleState() {

        if (breakStates())
            return;

        if (wantsToMove()) {
            playerCurrentState = PLAYER_STATES.WALK;

        } 
        else if (input.aimButtonDown) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM;

        }
        else if (input.fireButton) {
            playerCurrentState = PLAYER_STATES.IDLE_FIRE;

        }
    }


    private void idleFireState() {

        // Recoil Multiplier for idleFiring
        recoilMultiplier = recoilConfig.idleFireRecoilMultiplier;

        if (breakStates())
            return;

        if (gun.triggerPulled && wantsToMove()) {
            playerCurrentState = PLAYER_STATES.WALK_FIRE;

        } else if (!input.fireButton) {
            playerCurrentState = PLAYER_STATES.IDLE;
        }
        else if (gunAim.isAiming && gun.triggerPulled && !wantsToMove()) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM_FIRE;

        }
    }


    private void idleAimState() {

        // Can't jump while aiming
        input.jump = false;

        // Can't run while aiming
        input.dash = false;

        if (input.reload) {
            playerCurrentState = PLAYER_STATES.RELOAD;
        }
        else if (gunAim.isAiming && wantsToMove()) {
            playerCurrentState = PLAYER_STATES.WALK_AIM;

        } else if (gunAim.isAiming && gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM_FIRE;

        }
        else if (!gunAim.isAiming) {
            playerCurrentState = PLAYER_STATES.IDLE;
        }
    }


    private void idleAimFireState() {

        // Can't jump while aiming
        input.jump = false;

        // Can't run while aiming
        input.dash = false;

        // Recoil Multiplier for idleFiring
        recoilMultiplier = recoilConfig.idleAimFireRecoilMultiplier;

        if (input.reload) {
            playerCurrentState = PLAYER_STATES.RELOAD;

        }
        else if (gunAim.isAiming && gun.triggerPulled && wantsToMove()) {
            playerCurrentState = PLAYER_STATES.WALK_AIM_FIRE;

        }
        else if (gunAim.isAiming && !gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM;

        }
        else if (!gunAim.isAiming && !wantsToMove()) {
            playerCurrentState = PLAYER_STATES.IDLE_FIRE;
        }

    }


    private void walkState() {

        if (breakStates())
            return;

        if (wantsToMove() && gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.WALK_FIRE;

        } else if (wantsToMove() && gunAim.isAiming) {
            playerCurrentState = PLAYER_STATES.WALK_AIM;

        } else if (!wantsToMove()) {
            playerCurrentState = PLAYER_STATES.IDLE;

        }

    }


    private void walkFireState() {

        // Can't run while aiming
        input.dash = false;

        // Recoil Multiplier for walkFiring from hip
        recoilMultiplier = recoilConfig.walkFireRecoilMultiplier;

        if (wantsToMove() && !gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.WALK;

        }
        else if (!wantsToMove() && gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.IDLE_FIRE;

        }
        else if (wantsToMove() && gunAim.isAiming && gun.triggerPulled){
            playerCurrentState = PLAYER_STATES.WALK_AIM_FIRE;
            
        }
    }


    private void walkAimState() {

        // Can't jump while aiming
        input.jump = false;

        // Can't run while aiming
        input.dash = false;

        if (input.reload) {
            playerCurrentState = PLAYER_STATES.RELOAD;

        }
        else if (wantsToMove() && !gunAim.isAiming) {
            playerCurrentState = PLAYER_STATES.WALK;

        }else if(wantsToMove() && gunAim.isAiming && gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.WALK_AIM_FIRE;

        }
        else if (!wantsToMove() && gunAim.isAiming) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM;

        }

    }


    private void walkAimFireState() {

        // Can't jump while aiming
        input.jump = false;

        // Can't run while aiming
        input.dash = false;

        // Recoil Multiplier for walk aiming fire
        recoilMultiplier = recoilConfig.walkAimFireRecoilMultiplier;

        if (input.reload) {
            playerCurrentState = PLAYER_STATES.RELOAD;

        }
        else if (gunAim.isAiming && wantsToMove() && !gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.WALK_AIM;

        } else if (!wantsToMove() && gunAim.isAiming && gun.triggerPulled) {
            playerCurrentState = PLAYER_STATES.IDLE_AIM_FIRE;

        }
        else if (wantsToMove() && gun.triggerPulled && !gunAim.isAiming)      {
            playerCurrentState = PLAYER_STATES.WALK_FIRE;

        }

    }

    private void runState() {

        if (input.jump) {
            playerCurrentState = PLAYER_STATES.JUMP;

        } else if (gunAim.isAiming) {
            playerCurrentState = PLAYER_STATES.WALK_AIM;

        } else if (input.fireButton) {
            playerCurrentState = PLAYER_STATES.WALK_FIRE;

        } else if (!input.dash) {
            playerCurrentState = PLAYER_STATES.WALK;
        } 
    }


    private void jumpState() {
        
        // Don´t handle next state until the character is grounded
        if (!characterMovement.isGrounded)
            return;

        if (wantsToMove()) {
            playerCurrentState = PLAYER_STATES.WALK;

        } else if (!wantsToMove()) {
            playerCurrentState = PLAYER_STATES.IDLE;
        }

    }


    public void reload() {



    }


    public struct InputData {
        // Basic Movement
        public float hMovement;
        public float vMovement;

        // Mouse rotation
        public float verticalMouse;
        public float horizontalMouse;

        // Extra movement
        public bool dash;
        public bool jump;

        // Aiming
        public bool aimButtonDown;
        public bool aimButtonUp;

        // Firing
        public bool fireButton;
        public bool reload;

        public void getInput() {//aqui
            // Basic Movement
            hMovement = Input.GetAxisRaw("Horizontal");
            vMovement = Input.GetAxisRaw("Vertical");

            // Mouse rotation
            verticalMouse = Input.GetAxis("Mouse Y");
            horizontalMouse = Input.GetAxis("Mouse X");

            // Extra movement
            dash = Input.GetButton("Dash");
            jump = Input.GetButtonDown("Jump");

            // Aiming
            aimButtonDown = Input.GetButtonDown("Fire2");
            aimButtonUp = Input.GetButtonUp("Fire2");

            // Firing
            fireButton = Input.GetButton("Fire1");
           // reload = Input.GetButtonDown("Reload");
        }
    }


    [System.Serializable]
    public struct RecoilConfig {
        [Range(0f, 1f)]
        public float idleFireRecoilMultiplier;
        [Range(0f, 1f)]
        public float idleAimFireRecoilMultiplier;
        [Range(0f, 1f)]
        public float walkFireRecoilMultiplier;
        [Range(0f, 1f)]
        public float walkAimFireRecoilMultiplier;
    }
}




