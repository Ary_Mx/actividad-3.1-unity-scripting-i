﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZObjectPools;

    public class Activate_Object : MonoBehaviour {

    public EZObjectPools.EZObjectPool activation;
    public int tiempo_Spawn=1;
	// Use this for initialization
	void Start () {
        
    }
    void Update () {
        for (int i = 0; i < activation.PoolSize; i++)
        {
            if (!activation.ObjectList[i].activeSelf)
            {
                StartCoroutine(active_again(i));
            }

        }
    }
    IEnumerator active_again(int i)
    {
        yield return new WaitForSeconds(tiempo_Spawn);// cuanto tiempo quiero que salgan
        activation.ObjectList[i].SetActive(true);

    }
}

