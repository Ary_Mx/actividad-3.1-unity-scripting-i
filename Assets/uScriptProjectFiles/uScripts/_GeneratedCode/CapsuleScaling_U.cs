//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleScaling_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 Axes = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_13_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_14_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_4_System_Single = (float) 0;
   System.Single local_5_System_Single = (float) 0;
   UnityEngine.Vector3 local_6_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   public System.Single ScaleUnits = (float) 1;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_7 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1;
   bool logic_uScriptAct_GetDeltaTime_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetGameObjectScale logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_9 = new uScriptAct_GetGameObjectScale( );
   UnityEngine.GameObject logic_uScriptAct_GetGameObjectScale_Target_9 = default(UnityEngine.GameObject);
   UnityEngine.Vector3 logic_uScriptAct_GetGameObjectScale_Scale_9;
   System.Single logic_uScriptAct_GetGameObjectScale_X_9;
   System.Single logic_uScriptAct_GetGameObjectScale_Y_9;
   System.Single logic_uScriptAct_GetGameObjectScale_Z_9;
   bool logic_uScriptAct_GetGameObjectScale_Out_9 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_10 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_10 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_10 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_10;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_10;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_10 = true;
   //pointer to script instanced logic node
   uScriptAct_AddVector3_v2 logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_11 = new uScriptAct_AddVector3_v2( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_A_11 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_B_11 = new Vector3( );
   UnityEngine.Vector3 logic_uScriptAct_AddVector3_v2_Result_11;
   bool logic_uScriptAct_AddVector3_v2_Out_11 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_12 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_12 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_12 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_12;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_SetGameObjectScale logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_15 = new uScriptAct_SetGameObjectScale( );
   UnityEngine.GameObject[] logic_uScriptAct_SetGameObjectScale_Target_15 = new UnityEngine.GameObject[] {};
   UnityEngine.Vector3 logic_uScriptAct_SetGameObjectScale_Scale_15 = new Vector3( );
   bool logic_uScriptAct_SetGameObjectScale_Out_15 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_8 = new Vector3( );
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_8 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_7 || false == m_RegisteredForEvents )
      {
         owner_Connection_7 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.SetParent(g);
      logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_9.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_10.SetParent(g);
      logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_11.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_12.SetParent(g);
      logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_15.SetParent(g);
      owner_Connection_7 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("bf1bc3f5-2e69-44f6-87df-74e2bdf29206", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_clampVector3_8();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("bf1bc3f5-2e69-44f6-87df-74e2bdf29206", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("bf1bc3f5-2e69-44f6-87df-74e2bdf29206", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("85c81335-6949-4328-8167-34bb38abb789", "Get_Delta_Time", Relay_In_1)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_1, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1);
         local_4_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_1;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.Out;
         
         if ( test_0 == true )
         {
            Relay_In_10();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_clampVector3_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("38f33814-8809-42cd-829f-497ff3a90933", "capsule_movement_C", Relay_clampVector3_8)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_8 = Axes;
               
            }
            {
            }
         }
         method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_8 = capsule_movement_C.clampVector3(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_target_8);
         Axes = method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_Return_8;
         Relay_In_1();
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at capsule_movement_C.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("40e2d28c-6e27-4903-ac6c-10790ffeccb1", "Get_Scale", Relay_In_9)) return; 
         {
            {
               logic_uScriptAct_GetGameObjectScale_Target_9 = owner_Connection_7;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_9.In(logic_uScriptAct_GetGameObjectScale_Target_9, out logic_uScriptAct_GetGameObjectScale_Scale_9, out logic_uScriptAct_GetGameObjectScale_X_9, out logic_uScriptAct_GetGameObjectScale_Y_9, out logic_uScriptAct_GetGameObjectScale_Z_9);
         local_6_UnityEngine_Vector3 = logic_uScriptAct_GetGameObjectScale_Scale_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetGameObjectScale_uScriptAct_GetGameObjectScale_9.Out;
         
         if ( test_0 == true )
         {
            Relay_In_12();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Get Scale.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_10()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("b0e79104-baca-4956-abc8-0749d64d3c77", "Multiply_Float", Relay_In_10)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_10 = ScaleUnits;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_10 = local_4_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_10.In(logic_uScriptAct_MultiplyFloat_v2_A_10, logic_uScriptAct_MultiplyFloat_v2_B_10, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_10, out logic_uScriptAct_MultiplyFloat_v2_IntResult_10);
         local_5_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_10;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_10.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_11()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("f0c0dcce-07fa-4f2a-ae3e-f97c810658cc", "Add_Vector3", Relay_In_11)) return; 
         {
            {
               logic_uScriptAct_AddVector3_v2_A_11 = local_6_UnityEngine_Vector3;
               
            }
            {
               logic_uScriptAct_AddVector3_v2_B_11 = local_13_UnityEngine_Vector3;
               
            }
            {
            }
         }
         logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_11.In(logic_uScriptAct_AddVector3_v2_A_11, logic_uScriptAct_AddVector3_v2_B_11, out logic_uScriptAct_AddVector3_v2_Result_11);
         local_14_UnityEngine_Vector3 = logic_uScriptAct_AddVector3_v2_Result_11;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_AddVector3_v2_uScriptAct_AddVector3_v2_11.Out;
         
         if ( test_0 == true )
         {
            Relay_In_15();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Add Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_12()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d44011b9-2016-4f49-9946-9b71d9a2f425", "Multiply_Vector3_With_Float", Relay_In_12)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_12 = Axes;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_12 = local_5_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_12.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_12, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_12, out logic_uScriptAct_MultiplyVector3WithFloat_Result_12);
         local_13_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_12;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_12.Out;
         
         if ( test_0 == true )
         {
            Relay_In_11();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_15()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("a42ee94d-a097-4000-96f2-29f765be0bbb", "Set_Scale", Relay_In_15)) return; 
         {
            {
               int index = 0;
               if ( logic_uScriptAct_SetGameObjectScale_Target_15.Length <= index)
               {
                  System.Array.Resize(ref logic_uScriptAct_SetGameObjectScale_Target_15, index + 1);
               }
               logic_uScriptAct_SetGameObjectScale_Target_15[ index++ ] = owner_Connection_7;
               
            }
            {
               logic_uScriptAct_SetGameObjectScale_Scale_15 = local_14_UnityEngine_Vector3;
               
            }
         }
         logic_uScriptAct_SetGameObjectScale_uScriptAct_SetGameObjectScale_15.In(logic_uScriptAct_SetGameObjectScale_Target_15, logic_uScriptAct_SetGameObjectScale_Scale_15);
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleScaling_U.uscript at Set Scale.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:Axes", Axes);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "2902576a-60a1-4185-90a8-9591e0e5d2d8", Axes);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:ScaleUnits", ScaleUnits);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "0274c085-7614-4560-ab68-e7996da7338e", ScaleUnits);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:4", local_4_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "16f5789d-b4f7-4b6e-bee5-9d578f83c52d", local_4_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:5", local_5_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "aecb83b5-071f-4199-826f-bdb28d84e3b6", local_5_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:6", local_6_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "dc4c9738-725e-4a76-90e7-60da09936e1a", local_6_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:13", local_13_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "e087f05a-f59d-47ce-b645-7297bc317fcf", local_13_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleScaling_U.uscript:14", local_14_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "3ba06c42-8b43-4083-9f41-f18667fa6562", local_14_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
