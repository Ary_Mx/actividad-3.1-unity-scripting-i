//uScript Generated Code - Build 1.0.3104
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class CapsuleRotation_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_8_System_Single = (float) 0;
   System.Single local_9_System_Single = (float) 0;
   public System.Single Speed = (float) 1;
   public UnityEngine.Vector3 Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_11 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1;
   bool logic_uScriptAct_GetDeltaTime_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_2 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_XMin_2 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_XMax_2 = (float) 90;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_YMin_2 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_YMax_2 = (float) 90;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_2 = (bool) true;
   System.Single logic_uScriptAct_ClampVector3_ZMin_2 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_ZMax_2 = (float) 90;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_2;
   bool logic_uScriptAct_ClampVector3_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_6 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_6 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_6;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_6 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = new Vector3( );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_7 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_3 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_11 || false == m_RegisteredForEvents )
      {
         owner_Connection_11 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.SetParent(g);
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.SetParent(g);
      owner_Connection_11 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("ddc81d04-e664-45ec-b7cf-390714cf36da", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_2();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("ddc81d04-e664-45ec-b7cf-390714cf36da", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("ddc81d04-e664-45ec-b7cf-390714cf36da", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("d23dfcac-f45f-479e-b458-5856acc6c3b4", "Get_Delta_Time", Relay_In_1)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_1, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_1, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_1);
         local_8_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_1;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_1.Out;
         
         if ( test_0 == true )
         {
            Relay_In_6();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleRotation_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_2()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("0c7ba598-7486-4747-8320-48e453f658c1", "Clamp_Vector3", Relay_In_2)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_2 = Vector3;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.In(logic_uScriptAct_ClampVector3_Target_2, logic_uScriptAct_ClampVector3_ClampX_2, logic_uScriptAct_ClampVector3_XMin_2, logic_uScriptAct_ClampVector3_XMax_2, logic_uScriptAct_ClampVector3_ClampY_2, logic_uScriptAct_ClampVector3_YMin_2, logic_uScriptAct_ClampVector3_YMax_2, logic_uScriptAct_ClampVector3_ClampZ_2, logic_uScriptAct_ClampVector3_ZMin_2, logic_uScriptAct_ClampVector3_ZMax_2, out logic_uScriptAct_ClampVector3_Result_2);
         Vector3 = logic_uScriptAct_ClampVector3_Result_2;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_2.Out;
         
         if ( test_0 == true )
         {
            Relay_In_1();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleRotation_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Rotate_3()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("2ee762b6-d7be-4973-89a2-1399bdda6d78", "UnityEngine_Transform", Relay_Rotate_3)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_3 = local_10_UnityEngine_Vector3;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_11.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Rotate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_eulers_3);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleRotation_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_6()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("585c691f-f51b-4b81-a0c7-37bbb55d8676", "Multiply_Float", Relay_In_6)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_6 = Speed;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_6 = local_8_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.In(logic_uScriptAct_MultiplyFloat_v2_A_6, logic_uScriptAct_MultiplyFloat_v2_B_6, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_6, out logic_uScriptAct_MultiplyFloat_v2_IntResult_6);
         local_9_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_6;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_6.Out;
         
         if ( test_0 == true )
         {
            Relay_In_7();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleRotation_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_7()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("8b40a2eb-435e-48ec-9b3c-7759f6b1e215", "Multiply_Vector3_With_Float", Relay_In_7)) return; 
         {
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7 = Vector3;
               
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7 = local_9_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_7, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_7, out logic_uScriptAct_MultiplyVector3WithFloat_Result_7);
         local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_7;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_7.Out;
         
         if ( test_0 == true )
         {
            Relay_Rotate_3();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleRotation_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleRotation_U.uscript:Speed", Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d463c66b-1506-4ac7-8e92-f4b5308879e0", Speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleRotation_U.uscript:Vector3", Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6da9db28-0ff3-447a-88e8-a7dd54ed1567", Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleRotation_U.uscript:8", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "d6ed561e-5216-46f4-a7ac-5b1799c10150", local_8_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleRotation_U.uscript:9", local_9_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b826e364-141e-4d8d-b76a-25a45f590ade", local_9_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleRotation_U.uscript:10", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "15e6fbed-f9c5-401f-9be8-230eade9370e", local_10_UnityEngine_Vector3);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
