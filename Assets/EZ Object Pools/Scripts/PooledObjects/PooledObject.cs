﻿using UnityEngine;
using System.Collections;

namespace EZObjectPools
{
    [AddComponentMenu("EZ Object Pools/Pooled Object")]
    public class PooledObject : MonoBehaviour
    {
        /// <summary>
        /// The object pool this object originated from.
        /// </summary>
        [HideInInspector]
        public EZObjectPool ParentPool;
        /// <summary>
        /// [OBSOLETE] Simply calls gameObject.SetActive(false). No longer needed in your scripts.
        /// </summary>
        public virtual void Disable()
        {
            gameObject.SetActive(false);
        }

        void OnDisable()
        {
            //this.gameObject.SetActive(true);
           // int random_number;
           // random_number=Random.Range(1,3);
            if (this.gameObject.name.Equals("Enemy_type4(Clone)"))
            {
                transform.position = new Vector3(0, 0, -2.89f);// despues de desactivarse donde apareceran
            }
           // if (this.gameObject.name.Equals("Enemy_type3(Clone)")&&random_number==1)
           // {
           //     print("hola es uno");
           //     transform.position = new Vector3(11.5f,5f,84.56f);// despues de desactivarse donde apareceran
           // }
            if (this.gameObject.name.Equals("Enemy_type3(Clone)"))
            {
               // print("hola es dos");
                transform.position = new Vector3(22.18f,-2,0.92f);// despues de desactivarse donde apareceran
            }

            if (this.gameObject.name.Equals("Enemy_type2(Clone)"))
            {
                transform.position = new Vector3(0, 0, -2.89f);// despues de desactivarse donde apareceran
            }
            if (this.gameObject.name.Equals("Enemy_type1(Clone)"))
            {
                transform.position = new Vector3(-71.85f,-12.29f,14.96f);// despues de desactivarse donde apareceran
            }
            if (ParentPool)
                ParentPool.AddToAvailableObjects(this.gameObject);
            else
                Debug.LogWarning("PooledObject " + gameObject.name + " does not have a parent pool. If this occurred during a scene transition, ignore this. Otherwise reoprt to developer.");

        }
        
       
    }
}